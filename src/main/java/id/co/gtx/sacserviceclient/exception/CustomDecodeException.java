package id.co.gtx.sacserviceclient.exception;

import lombok.Getter;

public class CustomDecodeException extends RuntimeException{

    @Getter
    private String code;

    public CustomDecodeException(String message, String code) {
        super(message);
        this.code = code;
    }

    public CustomDecodeException(String message, String code, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}

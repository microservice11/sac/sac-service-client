package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;
import id.co.gtx.sacserviceclient.service.CommandTL1;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("fHCommand")
public class FHCommand extends DefaultCommandTL1 implements CommandTL1 {

    @Override
    public DtoCmdDetail inet(DtoInet dtoInet) {
        if (typeOnt != 1)
            return inetOpen(dtoInet);

        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoInet.getStatus();
        if (status == 1 || status == 2) {
            for (DtoData data : dtoInet.getData()) {
                Integer mode = data.getMode();
                Integer connType = data.getConnType();
                Integer nat = 1;
                if (mode == 1 || mode == 3 || mode == 5 || mode == 6 || mode == 8 || mode == 9 || mode == 10) {
                    if (mode == 1 || mode == 3 || mode == 6 || mode == 8 || mode == 9 || mode == 10) {
                        connType = 2;
                        if (mode == 1 || mode == 8)
                            nat = 2;
                    } else {
                        nat = 2;
                    }
                }
                Integer ipMode = data.getIpMode();
                Integer vlan = data.getVlan();
                String acc = data.getAcc();
                Integer qos = data.getQos();
                String inet = !"".equals(acc) && acc != null ? acc.split("@")[0] : "";
                String pwd = data.getPwd();
                String cmd = "SET-WANSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::";
                if (status == 1) {
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    String cmd_bw = ",UPPROFILENAME=UP-" + bwUp + ",DOWNPROFILENAME=DOWN-" + bwDown + ";";
                    if ("BYPASS".equals(bwUp) && "BYPASS".equals(bwDown)) {
                        cmd_bw = ";";
                    }
                    String cmd_end = cmd_bw;
                    cmd += "STATUS=" + status + ",MODE=" + mode + ",CONNTYPE=" + connType + ",VLAN=" + vlan + ",COS=" + qos + ",NAT=" + nat + ",IPMODE=" + ipMode;
                    String cmdLan;
                    for (Integer lan : data.getLan()) {
                        if (ipMode == 3)
                            cmdLan = cmd + ",PPPOEPROXY=1,PPPOEUSER=" + acc + ",PPPOEPASSWD=" + pwd + ",PPPOENAME=" + inet + ",PPPOEMODE=1,UPORT=" + lan + cmd_end;
                        else
                            cmdLan = cmd + ",UPORT=" + lan + cmd_end;
                        cmdList.add(cmdLan);
                    }
                    String cmdSsid;
                    for (Integer ssid : data.getSsid()) {
                        if (ipMode == 3)
                            cmdSsid = cmd + ",PPPOEPROXY=1,PPPOEUSER=" + acc + ",PPPOEPASSWD=" + pwd + ",PPPOENAME=" + inet + ",PPPOEMODE=1,SSID=" + ssid + cmd_end;
                        else
                            cmdSsid = cmd + ",SSID=" + ssid + cmd_end;
                        cmdList.add(cmdSsid);
                    }
                } else {
                    cmd += "STATUS=" + status + ",MODE=" + mode + ",CONNTYPE=" + connType + ",VLAN=" + vlan + ";";
                    cmdList.add(cmd);
                }
            }

            desc = status == 1 ? "CONFIG INTERNET" : "DELETE INTERNET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    private DtoCmdDetail inetOpen(DtoInet dtoInet) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoInet.getStatus();
        if ((status == 1 || status == 2) && !dtoInet.getData().isEmpty()) {
            DtoData data = dtoInet.getData().get(0);
            Integer vlan = data.getVlan();
            String cmd;
            if (status == 1) {
                String bwUp = data.getBwUp();
                String bwDown = data.getBwDown();
                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=200,SVCMODPROFILE=TR069_VEIP_PROFILE;";
                cmdList.add(cmd);

                cmd = "CFG-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=200:::BW=DOWN-" + bwDown + ";";
                cmdList.add(cmd);

                cmd = "CFG-ONUBWPROFILE::OLTID=" + ipGpon +",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::GPONSERVICEBW=UP-" + bwUp + ";";
                cmdList.add(cmd);
            } else {
                cmd = "DEL-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=200:::;";
                cmdList.add(cmd);

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=200,SVCMODPROFILE=TR069_VEIP_PROFILE;";
                cmdList.add(cmd);
            }

            desc = status == 1 ? "CONFIG INTERNET" : "DELETE INTERNET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail voip(DtoVoip dtoVoip) {
        if (typeOnt != 1)
            return voipOpen(dtoVoip);

        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoVoip.getStatus();
        Integer vlan = dtoVoip.getVlan();
        if (status == 1 || status == 2) {
            dtoVoip.getData().forEach(data -> {
                Integer post = data.getPots();
                String cmd;
                if (status == 1) {
                    String acc = data.getAcc();
                    String pwd = data.getPwd();
                    cmd = "CFG-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::PHONENUMBER=" + acc + ",PT=SIP,VOIPVLAN=" + vlan + ",SCOS=7,EID=@telkom.net.id,SIPUSERNAME=" + acc + ",SIPUSERPWD=" + pwd + ",IPMODE=DHCP;";
                } else {
                    cmd = "DEL-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::;";
                }
                cmdList.add(cmd);
            });
            String cmd;
            if (status == 1) {
                cmd = "SET-WANSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::STATUS=" + status + ",MODE=5,CONNTYPE=2,VLAN=" + vlan + ",COS=5,NAT=2,IPMODE=1,WANSVC=1;";
            } else {
                cmd = "SET-WANSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::STATUS=" + status + ",MODE=5,CONNTYPE=2,VLAN=" + vlan + ";";
            }
            cmdList.add(cmd);

            desc = status == 1 ? "CONFIG VOIP" : "DELETE VOIP";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    private DtoCmdDetail voipOpen(DtoVoip dtoVoip) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoVoip.getStatus();
        if (status == 1 || status == 2) {
            Integer vlan = dtoVoip.getVlan();
            String cmd;
            if (status == 1) {
                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=100,SVCMODPROFILE=TR069_VEIP_PROFILE;";
                cmdList.add(cmd);

                cmd = "CFG-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=100:::BW=DOWN-564KF5;";
                cmdList.add(cmd);

                cmd = "CFG-ONUBWPROFILE::OLTID=" + ipGpon +",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::GPONSERVICEBW=UP-564KF5;";
                cmdList.add(cmd);
            } else {
                cmd = "DEL-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=100:::;";
                cmdList.add(cmd);

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=100,SVCMODPROFILE=TR069_VEIP_PROFILE;";
                cmdList.add(cmd);
            }

            desc = status == 1 ? "CONFIG VOIP" : "DELETE VOIP";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail iptv(DtoIptv dtoIptv) {
        if (typeOnt != 1)
            return iptvOpen(dtoIptv);

        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoIptv.getStatus();
        if (status == 1 || status == 2) {
            Arrays.stream(dtoIptv.getLan()).iterator().forEachRemaining(lan -> {
                String cmd;
                if (status == 1) {
                    cmd = "ADD-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::MVLAN=110,MCOS=4;";
                    cmdList.add(cmd);

                    cmd = "CFG-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::CVLAN=111,CCOS=4;";
                    cmdList.add(cmd);

                    cmd = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::BW=DOWN8MKA4_UP2253KA4;";
                    cmdList.add(cmd);
                } else {
                    cmd = "DEL-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::MVLAN=110;";
                    cmdList.add(cmd);

                    cmd = "DEL-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::UV=111;";
                    cmdList.add(cmd);
                }
            });
            desc = status == 1 ? "CONFIG USEETV" : "DELETE USEETV";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    private DtoCmdDetail iptvOpen(DtoIptv dtoIptv) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoIptv.getStatus();
        if (status == 1 || status == 2) {
            String cmd;
            if (status == 1) {
                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=110,UV=110,SVCMODPROFILE=Multicast;";
                cmdList.add(cmd);

                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=111,UV=111,SVCMODPROFILE=IGMP;";
                cmdList.add(cmd);

                cmd = "CFG-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",UV=110:::BW=DOWN8MKA4;";
                cmdList.add(cmd);

                cmd = "CFG-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",UV=111:::BW=DOWN8MKA4;";
                cmdList.add(cmd);

                cmd = "CFG-ONUBWPROFILE::OLTID=" + ipGpon +",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::GPONSERVICEBW=UP-UP2253KA4;";
                cmdList.add(cmd);
            } else {
                cmd = "DEL-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=111,UV=111:::;";
                cmdList.add(cmd);

                cmd = "DEL-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=110,UV=110:::;";
                cmdList.add(cmd);

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=111,UV=111,SVCMODPROFILE=IGMP;";
                cmdList.add(cmd);

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=110,UV=110,SVCMODPROFILE=Multicast;";
                cmdList.add(cmd);
            }

            desc = status == 1 ? "CONFIG USEETV" : "DELETE USEETV";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail other(DtoOther dtoOther) {
        if (typeOnt != 1)
            return otherOpen(dtoOther);

        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoOther.getStatus();
        if (status == 1 || status == 2) {
            dtoOther.getData().forEach(data -> {
                Integer vlan = data.getVlan();
                Arrays.stream(data.getLan()).iterator().forEachRemaining(lan -> {
                    String cmd;
                    if (status == 1)
                        cmd = "CFG-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::CVLAN=" + vlan + ",CCOS=1;";
                    else
                        cmd = "DEL-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::UV=" + vlan + ";";
                    cmdList.add(cmd);
                });
            });
            desc = status == 1 ? "CONFIG WIFIID/VPNID/ASTINET" : "DELETE WIFIID/VPNID/ASTINET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    private DtoCmdDetail otherOpen(DtoOther dtoOther) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoOther.getStatus();
        if (status == 1 || status == 2) {
            dtoOther.getData().forEach(data -> {
                Integer vlan = data.getVlan();
                Integer svlan = data.getSvlan();
                String cmd;
                if (status == 1) {
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SVCMODPROFILE=TR069_VEIP_PROFILE;";
                    cmdList.add(cmd);

                    cmd = "CFG-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=" + svlan + ":::BW=DOWN-" + bwDown + ";";
                    cmdList.add(cmd);

                    cmd = "CFG-ONUBWPROFILE::OLTID=" + ipGpon +",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::GPONSERVICEBW=UP-" + bwUp + ";";
                    cmdList.add(cmd);
                } else {
                    cmd = "DEL-QINQLIMIT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",CVLAN=" + vlan + ",UV=" + svlan + ":::;";
                    cmdList.add(cmd);

                    cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SVCMODPROFILE=TR069_VEIP_PROFILE;";
                    cmdList.add(cmd);
                }
            });

            desc = status == 1 ? "CONFIG WIFIID/VPNID/ASTINET" : "DELETE WIFIID/VPNID/ASTINET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }
}

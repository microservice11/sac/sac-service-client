package id.co.gtx.sacserviceclient.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacmodules.enumz.Protocol;
import id.co.gtx.sacmodules.enumz.Status;
import id.co.gtx.sacmodules.enumz.Vendor;
import id.co.gtx.sacserviceclient.dto.DtoCmdBatch;
import id.co.gtx.sacserviceclient.dto.DtoCmdBatchDetail;
import id.co.gtx.sacserviceclient.dto.DtoCmdCheck;
import id.co.gtx.sacserviceclient.service.BaseCommandTL1;
import id.co.gtx.sacserviceclient.service.CommandCheckTL1;
import id.co.gtx.sacserviceclient.service.CommandTL1;
import id.co.gtx.sacserviceclient.service.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;

@Slf4j
@Component
public class ConfigServiceImpl implements ConfigService {

    private final CommandTL1 fHCommand;

    private final CommandTL1 zteCommand;

    private final CommandTL1 aluCommand;

    private final CommandCheckTL1 commandCheckTl1;

    private final CommandCheckTL1 commandCheckSsh;

    private final BaseCommandTL1 defaultCommand;

    private final FileStorageServiceImpl fileStorageService;

    public ConfigServiceImpl(CommandTL1 fHCommand,
                             CommandTL1 zteCommand,
                             CommandTL1 aluCommand,
                             CommandCheckTL1 commandCheckTl1,
                             CommandCheckTL1 commandCheckSsh,
                             BaseCommandTL1 defaultCommand,
                             FileStorageServiceImpl fileStorageService) {
        this.fHCommand = fHCommand;
        this.zteCommand = zteCommand;
        this.aluCommand = aluCommand;
        this.commandCheckTl1 = commandCheckTl1;
        this.commandCheckSsh = commandCheckSsh;
        this.defaultCommand = defaultCommand;
        this.fileStorageService = fileStorageService;
    }

    @Bean
    public Function<DtoProc<List<DtoConfig>>, DtoProc<List<DtoCmdBatch>>> configBatch() {
        return dtoProc -> configCreate(dtoProc.getConfigs(), dtoProc.getProccessId(), dtoProc.getUsername(), dtoProc.getIpCLient());
    }

    @Bean
    public Function<DtoProc<List<DtoConfig>>, DtoProc<List<DtoCmdCheck>>> checkUnreg() {
        return dtoProc -> checkOnt(dtoProc.getConfigs(), dtoProc.getProccessId(), dtoProc.getUsername(), dtoProc.getIpCLient(), dtoProc.getProtocol(), 1);
    }

    @Bean
    public Function<DtoProc<List<DtoConfig>>, DtoProc<List<DtoCmdCheck>>> checkReg() {
        return dtoProc -> checkOnt(dtoProc.getConfigs(), dtoProc.getProccessId(), dtoProc.getUsername(), dtoProc.getIpCLient(), dtoProc.getProtocol(), 2);
    }

    @Bean
    public Function<DtoProc<List<DtoConfig>>, DtoProc<List<DtoCmdCheck>>> checkService() {
        return dtoProc -> checkOnt(dtoProc.getConfigs(), dtoProc.getProccessId(), dtoProc.getUsername(), dtoProc.getIpCLient(), dtoProc.getProtocol(), 3);
    }

    @Bean
    public Function<DtoProc<List<DtoConfig>>, DtoProc<List<DtoCmdBatch>>> configCreate() {
        return dtoProc -> configCreate(dtoProc.getConfigs(), dtoProc.getProccessId(), dtoProc.getUsername(), dtoProc.getIpCLient());
    }

    @Override
    public ResponseEntity<Resource> downloadTemplate(String filename) {
        DtoResponse response = new DtoResponse("DOWNLOAD FILE", Status.FAILED, "Could not determine file type.");
        Resource resource = fileStorageService.loadFileAsResource(filename);

        String contentType = "application/vnd.ms-excel";
        response.setStatus(Status.SUCCESS);
        response.setMessage("Berhasil Download File");

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private DtoProc<List<DtoCmdBatch>> configCreate(List<DtoConfig> dtoConfigs, Long proccessId, String username, String ipClient) {
        Set<DtoNms> nmsSet = new HashSet<>();
        dtoConfigs.forEach(config -> nmsSet.add(config.getNms()));

        List<DtoCmdBatch> cmdBatches = new ArrayList<>();
        for (DtoNms nms : nmsSet) {
            DtoCmdBatch cmdBatch = new DtoCmdBatch();
            cmdBatch.setNms(nms);
            if (!nms.getVendor().equals(Vendor.ZTE)) {
                cmdBatch.setLogin(defaultCommand.login(nms));
                cmdBatch.setLogout(defaultCommand.logout());
            }

            List<DtoCmdBatchDetail> cmdBatchDetails = new ArrayList<>();
            for (DtoConfig config : dtoConfigs) {
                DtoCmdBatchDetail cmd = new DtoCmdBatchDetail();
                Integer status = config.getStatus();
                Vendor vendor = config.getNms().getVendor();
                String onuType = config.getOnuType();
                String onuName = config.getOnuName();
                DtoInet inet = config.getInet();
                DtoVoip voip = config.getVoip();
                DtoIptv iptv = config.getIptv();
                DtoOther other = config.getOther();
                if (nms.getVendor().equals(vendor) && nms.getIpServer().equals(config.getNms().getIpServer())) {
                    Map<String, String> map = new HashMap<>();
                    map.put("ONUTYPE", onuType);
                    map.put("ONUIDTYPE", "MAC");
                    cmd.setStatus(status);
                    if (nms.getVendor().equals(Vendor.FH)) {
                        fHCommand.setValue(config);
                        if (onuName != null)
                            cmd.setOnt(fHCommand.ont(status, map, onuName));
                        else
                            cmd.setOnt(fHCommand.ont(status, map));
                        if (inet != null)
                            cmd.setInet(fHCommand.inet(inet));
                        if (voip != null)
                            cmd.setVoip(fHCommand.voip(voip));
                        if (iptv != null)
                            cmd.setIptv(fHCommand.iptv(iptv));
                        if (other != null)
                            cmd.setOther(fHCommand.other(other));
                    } else if (nms.getVendor().equals(Vendor.ZTE)) {
                        zteCommand.setValue(config);
                        map.put("ONUIDTYPE", "SN");
                        if (onuName != null)
                            cmd.setOnt(zteCommand.ont(status, map, onuName));
                        else
                            cmd.setOnt(zteCommand.ont(status, map));
                        if (inet != null)
                            cmd.setInet(zteCommand.inet(inet));
                        if (voip != null)
                            cmd.setVoip(zteCommand.voip(voip));
                        if (iptv != null)
                            cmd.setIptv(zteCommand.iptv(iptv));
                        if (other != null)
                            cmd.setOther(zteCommand.other(other));
                    } else if (nms.getVendor().equals(Vendor.ALU)) {
                        aluCommand.setValue(config);
                        if (onuName != null)
                            cmd.setOnt(aluCommand.ont(status, map, onuName));
                        else
                            cmd.setOnt(aluCommand.ont(status, map));
                        if (inet != null)
                            cmd.setInet(aluCommand.inet(inet));
                        if (voip != null)
                            cmd.setVoip(aluCommand.voip(voip));
                        if (iptv != null)
                            cmd.setIptv(aluCommand.iptv(iptv));
                        if (other != null)
                            cmd.setOther(aluCommand.other(other));
                    }
                    cmdBatchDetails.add(cmd);
                }
            }
            cmdBatch.setDetails(cmdBatchDetails);
            cmdBatches.add(cmdBatch);
        }
        DtoProc<List<DtoCmdBatch>> dtoProc = new DtoProc<>();
        dtoProc.setProccessId(proccessId);
        dtoProc.setConfigs(cmdBatches);
        dtoProc.setUsername(username);
        dtoProc.setIpCLient(ipClient);
        dtoProc.setType(0);
        dtoProc.setTypeReq(4);

        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(dtoProc);
            log.info(json);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return dtoProc;
    }

    //typeCheck: 1 - UNREGISTER, 2 - REGISTER, 3 - SERVICE
    private DtoProc<List<DtoCmdCheck>> checkOnt(List<DtoConfig> dtoConfigs, Long proccessId, String username, String ipClient, Protocol protocol, int typeCheck) {
        List<DtoCmdCheck> cmdChecks;
        DtoProc<List<DtoCmdCheck>> dtoProc = new DtoProc<>();
        dtoProc.setProccessId(proccessId);
        dtoProc.setUsername(username);
        dtoProc.setIpCLient(ipClient);

        if (protocol.getValue().equals("SSH2")) {
            if (typeCheck == 1)
                cmdChecks = commandCheckSsh.checkUnreg(dtoConfigs);
            else if (typeCheck == 2)
                cmdChecks = commandCheckSsh.checkReg(dtoConfigs);
            else
                cmdChecks = commandCheckSsh.checkService(dtoConfigs);

            ObjectMapper mapper = new ObjectMapper();
            try {
                String json = mapper.writeValueAsString(cmdChecks);
                log.info(json);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            dtoProc.setConfigs(cmdChecks);
            dtoProc.setType(1);
            dtoProc.setTypeReq(typeCheck);
        } else {
            if (typeCheck == 1)
                cmdChecks = commandCheckTl1.checkUnreg(dtoConfigs);
            else if (typeCheck == 2)
                cmdChecks = commandCheckTl1.checkReg(dtoConfigs);
            else
                cmdChecks = commandCheckTl1.checkService(dtoConfigs);

            ObjectMapper mapper = new ObjectMapper();
            try {
                String json = mapper.writeValueAsString(cmdChecks);
                log.info(json);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            dtoProc.setConfigs(cmdChecks);
            dtoProc.setType(0);
            dtoProc.setTypeReq(0);
        }

        return dtoProc;
    }
}

package id.co.gtx.sacserviceclient.service;

import id.co.gtx.sacmodules.dto.DtoInet;
import id.co.gtx.sacmodules.dto.DtoIptv;
import id.co.gtx.sacmodules.dto.DtoOther;
import id.co.gtx.sacmodules.dto.DtoVoip;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;

public interface CommandTL1 extends BaseCommandTL1 {
    DtoCmdDetail inet(DtoInet dtoInet);

    DtoCmdDetail voip(DtoVoip dtoVoip);

    DtoCmdDetail iptv(DtoIptv dtoIptv);

    DtoCmdDetail other(DtoOther dtoOther);
}

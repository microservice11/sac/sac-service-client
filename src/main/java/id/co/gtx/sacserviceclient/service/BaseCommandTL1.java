package id.co.gtx.sacserviceclient.service;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoNms;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;

import java.util.Map;

public interface BaseCommandTL1 {
    void setValue(DtoConfig device);

    String login(DtoNms nms);

    String logout();

    DtoCmdDetail ont(Integer status, Map<String, String> onuType);

    DtoCmdDetail ont(Integer status, Map<String, String> onuType, String name);
}

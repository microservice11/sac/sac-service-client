package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoNms;
import id.co.gtx.sacserviceclient.dto.DtoCmdCheck;
import id.co.gtx.sacserviceclient.service.CommandCheckTL1;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("commandCheckSsh")
public class CommandCheckSSHImpl implements CommandCheckTL1 {

    @Override
    public List<DtoCmdCheck> checkUnreg(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "UNREG");
    }

    @Override
    public List<DtoCmdCheck> checkReg(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "REG");
    }

    @Override
    public List<DtoCmdCheck> checkService(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "SERVICE");
    }

    private List<DtoCmdCheck> check(List<DtoConfig> dtoConfigs, String type) {
        List<DtoCmdCheck> cmdChecks = new ArrayList<>();
        Set<DtoNms> ipServers = new HashSet<>();
        dtoConfigs.forEach(config -> {
            DtoNms nms = config.getNms();
            ipServers.add(nms);
        });

        ipServers.forEach(nms -> {
            DtoCmdCheck cmdCheck = new DtoCmdCheck();
            List<DtoConfig> gpons = new ArrayList<>();

            dtoConfigs.forEach(config -> {
                if (nms.getIpServer().equals(config.getNms().getIpServer())) {
                    DtoConfig gpon = new DtoConfig();
                    gpon.setIpGpon(config.getIpGpon());
                    gpon.setSlotPort(config.getSlotPort());
                    gpon.setOnuId(config.getOnuId());
                    gpon.setUsername(config.getUsername());
                    gpon.setPassword(config.getPassword());
                    gpons.add(gpon);
                }
            });
            cmdCheck.setNms(nms);
            cmdCheck.setGpon(gpons);
            cmdChecks.add(cmdCheck);
        });
        return cmdChecks;
    }
}

package id.co.gtx.sacserviceclient.service;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

public interface ConfigService {
    ResponseEntity<Resource> downloadTemplate(String filename);
}

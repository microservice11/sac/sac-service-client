package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;
import id.co.gtx.sacserviceclient.service.CommandTL1;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("aluCommand")
public class ALUCommand extends DefaultCommandTL1 implements CommandTL1 {

    @Override
    public DtoCmdDetail inet(DtoInet dtoInet) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoInet.getStatus();
        if (status == 1 || status == 2) {
            for (DtoData data : dtoInet.getData()) {
                Integer vlan = data.getVlan();
                String cmd;
                if (status == 1) {
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    if (typeOnt == 1) {
                        String acc = data.getAcc();
                        String pwd = data.getPwd();
                        cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=881,PPPOEUSER=" + acc + ",PPPOEPWD=" + pwd + ",UPBW_DOWNBW=UP-" + bwUp + "_DOWN-" + bwDown + ";";
                    } else {
                        cmd = "ADD-HSI::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=200,UPBW=UP-1G,DOWNBW=DOWN-1G,UPBW_DOWNBW=UP-" + bwUp + "_DOWN-" + bwDown + ";";
                    }
                } else {
                    if (typeOnt == 1) {
                        cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=881;";
                    } else {
                        cmd = "DEL-SERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=200;";
                    }
                }
                cmdList.add(cmd);
            }

            desc = status == 1 ? "CONFIG INTERNET" : "DELETE INTERNET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
            dtoCmdDetail.setSkipError(true);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail voip(DtoVoip dtoVoip) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoVoip.getStatus();
        if (status == 1 || status == 2) {
            dtoVoip.getData().forEach(data -> {
                Integer post = data.getPots();
                String cmd;
                if (status == 1) {
                    Integer vlan = dtoVoip.getVlan();
                    if (typeOnt == 1) {
                        String acc = data.getAcc();
                        String pwd = data.getPwd();
                        cmd = "CFG-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::PT=SIP,VOIPVLAN=" + vlan + ",UV=100,IPMODE=DHCP,PHONENUMBER=" + acc + ",SIPREGDM=10.0.0.11,SIPREGDM2=10.0.0.110,SIPUSERNAME=" + acc + "@telkom.net.id,SIPUSERPWD=" + pwd + ",UPBW_DOWNBW=UP-564KF5_DOWN-564KF5;";
                    } else {
                        cmd = "ADD-VOIP::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=100,UPBW=UP-1G,DOWNBW=DOWN-1G,UPBW_DOWNBW=UP-564KF5_DOWN-564KF5;";
                    }
                } else {
                    if (typeOnt == 1) {
                        cmd = "DEL-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::;";
                    } else {
                        cmd = "DEL-SERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=100;";
                    }
                }
                cmdList.add(cmd);
            });
            desc = status == 1 ? "CONFIG VOIP" : "DELETE VOIP";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
            dtoCmdDetail.setSkipError(true);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail iptv(DtoIptv dtoIptv) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoIptv.getStatus();
        if (status == 1 || status == 2) {
            Arrays.stream(dtoIptv.getLan()).iterator().forEachRemaining(lan -> {
                String cmd;
                if (status == 1) {
                    if (typeOnt == 1) {
                        cmd = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=NA-NA-NA-" + lan + ":::UPBW=UP-2253KA4,DOWNBW=DOWN-9012KA4;";
                        cmdList.add(cmd);

                        cmd = "CFG-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=NA-NA-NA-" + lan + ":::CVLAN=111,PVID=111;";
                        cmdList.add(cmd);

                        cmd = "ADD-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=NA-NA-NA-" + lan + ":::UV=111;";
                        cmdList.add(cmd);
                    } else {
                        cmd = "ADD-IPTV::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=111,UV=111,UPBW=UP-1G,DOWNBW=DOWN-1G,UPBW_DOWNBW=UP-2253KA4_DOWN-9012KA4;";
                        cmdList.add(cmd);

                        cmd = "ADD-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=111;";
                        cmdList.add(cmd);
                    }
                } else {
                    if (typeOnt == 1) {
                        cmd = "DEL-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::UV=111;";
                        cmdList.add(cmd);

                        cmd = "DEL-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::CVLAN=111,PVID=111;";
                        cmdList.add(cmd);
                    } else {
                        cmd = "DEL-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=111;";
                        cmdList.add(cmd);

                        cmd = "DEL-SERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=111;";
                        cmdList.add(cmd);
                    }
                }
            });
            desc = status == 1 ? "CONFIG USEETV" : "DELETE USEETV";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
            dtoCmdDetail.setSkipError(true);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail other(DtoOther dtoOther) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoOther.getStatus();
        if (status == 1 || status == 2) {
            dtoOther.getData().forEach(data -> {
                Integer vlan = data.getVlan();
                Integer svlan = typeOnt == 1 ? data.getVlan() : data.getSvlan();
                String cmd;
                if (status == 1) {
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    if (typeOnt == 1) {
                        Arrays.stream(data.getLan()).iterator().forEachRemaining(lan -> {
                            String cmdData = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=NA-NA-NA-" + lan + ":::UPBW=UP-" + bwUp + ",DOWNBW=DOWN-" + bwDown + ";";
                            cmdList.add(cmdData);

                            cmdData = "CFG-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=NA-NA-NA-" + lan + ":::CVLAN=" + vlan + ",PVID=" + svlan + ";";
                            cmdList.add(cmdData);
                        });
                    } else {
                        cmd = "ADD-HSI::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",UPBW=UP-1G,DOWNBW=DOWN-1G,UPBW_DOWNBW=UP-" + bwUp + "_DOWN-" + bwDown + ";";
                        cmdList.add(cmd);
                    }
                }else {
                    if (typeOnt == 1) {
                        Arrays.stream(data.getLan()).iterator().forEachRemaining(lan -> {
                            String cmdData = "DEL-LANPORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::CVLAN=" + vlan + ",PVID=" + svlan + ";";
                            cmdList.add(cmdData);
                        });
                    } else {
                        cmd = "DEL-SERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::UV=" + svlan + ";";
                        cmdList.add(cmd);
                    }
                }
            });

            desc = status == 1 ? "CONFIG WIFIID/VPNID/ASTINET" : "DELETE WIFIID/VPNID/ASTINET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
            dtoCmdDetail.setSkipError(true);
        }
        return dtoCmdDetail;
    }
}

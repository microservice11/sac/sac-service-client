package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoNms;
import id.co.gtx.sacserviceclient.dto.DtoCmdCheck;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;
import id.co.gtx.sacserviceclient.exception.CustomDecodeException;
import id.co.gtx.sacserviceclient.service.CommandCheckTL1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service("commandCheckTl1")
public class CommandCheck implements CommandCheckTL1 {

    @Value("${config.unreg.time-sleep:2}")
    private Integer configUnregTimeSleep;

    @Value("${config.reg.time-sleep:2}")
    private Integer configRegTimeSleep;

    @Value("${config.service.time-sleep:2}")
    private Integer configServiceTimeSleep;

    @Override
    public List<DtoCmdCheck> checkUnreg(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "UNREG");
    }

    @Override
    public List<DtoCmdCheck> checkReg(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "REG");
    }

    @Override
    public List<DtoCmdCheck> checkService(List<DtoConfig> dtoConfigs) {
        return this.check(dtoConfigs, "SERVICE");
    }

    private List<DtoCmdCheck> check(List<DtoConfig> dtoConfigs, String cmdType) {
        List<DtoCmdCheck> cmdChecks = new ArrayList<>();
        Set<DtoNms> ipServers = new HashSet<>();
        dtoConfigs.forEach(config -> {
            DtoNms nms = config.getNms();
            ipServers.add(nms);
        });
        String desc = "CEK UNREGISTER ONT";
        if (cmdType.equalsIgnoreCase("REG"))
            desc = "CEK REGISTER ONT";
        else if (cmdType.equalsIgnoreCase("SERVICE"))
            desc = "CEK SERVICE ONT";

        for (DtoNms nms: ipServers) {
            DtoCmdCheck cmdCheck = new DtoCmdCheck();
            List<String> cmdList = new ArrayList<>();
            String vendor = nms.getVendor().getValue();

            for (DtoConfig config : dtoConfigs) {
                if (nms.getIpServer().equals(config.getNms().getIpServer())) {
                    String ipGpon = config.getIpGpon();
                    String cmd;
                    if (cmdType.equalsIgnoreCase("UNREG")) {
                        cmd = "LST-UNREGONU::OLTID=" + ipGpon + ":::;";
                        cmdList.add(cmd);
                    } else if (cmdType.equalsIgnoreCase("REG")) {
                        cmd = "LST-ONU::OLTID=" + ipGpon;
                        if (config.getSlotPort() != null) {
                            String slotPort = config.getSlotPort();
                            cmd += ",PONID=1-1-" + slotPort;
                        }

                        if (config.getOnuId() != null) {
                            String onuId = config.getOnuId();
                            String onuIdType = vendor.equals("ZTE") ? "SN" : "MAC";
                            cmd += ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId;
                        }
                        cmd += ":::;";
                        cmdList.add(cmd);
                    } else if (cmdType.equalsIgnoreCase("SERVICE")) {
                        String slotPort = config.getSlotPort();
                        String onuId = config.getOnuId();

                        if (vendor.equals("ALU") || vendor.equals("FH") || vendor.equals("ZTE")) {
                            String onuIdType = vendor.equals("ZTE") ? "SN" : "MAC";

                            cmd = "LST-ONU::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId + ":::;";
                            cmdList.add(cmd);

                            cmd = "LST-ONUSTATE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId + ":::;";
                            cmdList.add(cmd);

                            if (vendor.equals("FH")) {
                                cmd = "LST-ONUWANSERVICECFG::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId + ":::;";
                                cmdList.add(cmd);

                                cmd = "LST-POTS::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ":::;";
                                cmdList.add(cmd);
                            } else if (vendor.equals("ZTE")) {
                                cmd = "LST-SERVICEPORT::DID=" + ipGpon + ",OID=" + onuId + ":::;";
                                cmdList.add(cmd);

                                cmd = "LST-ONUWANIP::DID=" + ipGpon + ",OID=" + onuId + ":::;";
                                cmdList.add(cmd);
                            }

                            cmd = "LST-POTSINFO::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId + ":::;";
                            cmdList.add(cmd);

                            cmd = "LST-PORTVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=" + onuIdType + ",ONUID=" + onuId + ":::;";
                            cmdList.add(cmd);
                        }
                    }
                }
            }
            if (!vendor.equals("ZTE")) {
                String username;
                String password;
                try {
                    username = new String(Base64.getDecoder().decode(nms.getUsername()));
                    password = new String(Base64.getDecoder().decode(nms.getPassword()));
                } catch (Exception e) {
                    throw new CustomDecodeException("Username atau Password Perangkat Belum Terenkripsi", "ERROR DECODE");
                }
                cmdCheck.setLogin("LOGIN:::5::UN=" + username + ",PWD=" + password + ";");
                cmdCheck.setLogout("LOGOUT:::5::;");
            }

            cmdCheck.setNms(nms);
            if ("UNREG".equals(cmdType)) {
                cmdCheck.setCmdList(new DtoCmdDetail(desc, cmdList, configUnregTimeSleep));
            } else if ("REG".equals(cmdType)) {
                cmdCheck.setCmdList(new DtoCmdDetail(desc, cmdList, configRegTimeSleep));
            } else if ("SERVICE".equals(cmdType)) {
                cmdCheck.setCmdList(new DtoCmdDetail(desc, cmdList, configServiceTimeSleep));
            }
            cmdChecks.add(cmdCheck);
        }
        return cmdChecks;
    }
}

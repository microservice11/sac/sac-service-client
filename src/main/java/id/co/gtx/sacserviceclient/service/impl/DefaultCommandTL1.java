package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoNms;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;
import id.co.gtx.sacserviceclient.exception.CustomDecodeException;
import id.co.gtx.sacserviceclient.service.BaseCommandTL1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Map;

@Slf4j
@Service("defaultCommand")
public class DefaultCommandTL1 implements BaseCommandTL1 {
    protected String ipGpon;
    protected String slotPort;
    protected String onuId;
    protected String target;
    protected Integer typeOnt;

    @Override
    public void setValue(DtoConfig device) {
        ipGpon = device.getIpGpon();
        slotPort = device.getSlotPort();
        onuId = device.getOnuId();
        target = "IP GPON: " + ipGpon + ", SLOT: " + slotPort.split("-")[0] + ", PORT: " + slotPort.split("-")[1] + ", SN: " + onuId;
        typeOnt = device.getOpenOnt();
    }

    @Override
    public String login(DtoNms nms) {
        String username;
        String password;
        try {
            username = new String(Base64.getDecoder().decode(nms.getUsername()));
            password = new String(Base64.getDecoder().decode(nms.getPassword()));
        } catch (Exception e) {
            throw new CustomDecodeException("Username atau Password Perangkat Belum Terenkripsi", "ERROR DECODE");
        }
        String cmd = "LOGIN:::5::UN=" + username + ",PWD=" + password + ";";
        return cmd;
    }

    @Override
    public String logout() {
        String cmd = "LOGOUT:::5::;";
        return cmd;
    }

    @Override
    public DtoCmdDetail ont(Integer status, Map<String, String> onuType) {
        String desc = null;
        String cmd = null;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        if (status == 1 || status == 2) {
            if (status == 1) {
                desc = "CONFIG ONT";
                cmd = "ADD-ONU::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ":::AUTHTYPE=" + onuType.get("ONUIDTYPE") + ",ONUID=" + onuId + ",ONUTYPE=" + onuType.get("ONUTYPE") + ";";
            } else {
                desc = "DELETE ONT";
                cmd = "DEL-ONU::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ":::ONUIDTYPE=" + onuType.get("ONUIDTYPE") + ",ONUID=" + onuId + ";";
            }
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmd(cmd);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail ont(Integer status, Map<String, String> onuType, String name) {
        String desc;
        String cmd;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        if (status == 1 || status == 2) {
            if (status == 1) {
                desc = "CONFIG ONT";
                cmd = "ADD-ONU::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ":::AUTHTYPE=" + onuType.get("ONUIDTYPE") + ",ONUID=" + onuId + ",ONUTYPE=" + onuType.get("ONUTYPE") + ",NAME=" + name + ";";
            } else {
                desc = "DELETE ONT";
                cmd = "DEL-ONU::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ":::ONUIDTYPE=" + onuType.get("ONUIDTYPE") + ",ONUID=" + onuId + ";";
            }
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmd(cmd);
        }
        return dtoCmdDetail;
    }
}

package id.co.gtx.sacserviceclient.service;

import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacserviceclient.dto.DtoCmdCheck;

import java.util.List;

public interface CommandCheckTL1 {
    List<DtoCmdCheck> checkUnreg(List<DtoConfig> dtoConfigs);

    List<DtoCmdCheck> checkReg(List<DtoConfig> dtoConfigs);

    List<DtoCmdCheck> checkService(List<DtoConfig> dtoConfigs);
}

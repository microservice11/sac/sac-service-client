package id.co.gtx.sacserviceclient.service.impl;

import id.co.gtx.sacmodules.dto.*;
import id.co.gtx.sacserviceclient.dto.DtoCmdDetail;
import id.co.gtx.sacserviceclient.service.CommandTL1;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("zteCommand")
public class ZTECommand extends DefaultCommandTL1 implements CommandTL1 {

    @Override
    public DtoCmdDetail inet(DtoInet dtoInet) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoInet.getStatus();
        if (status == 1 || status == 2) {
            for (DtoData data : dtoInet.getData()) {
                Integer vlan = data.getVlan();
                Integer svlan = typeOnt == 1 ? data.getVlan() : 200;
                Integer vport = dtoInet.getVport();
                String cmd;
                if (status == 1) {
                    String acc = data.getAcc();
                    String pwd = data.getPwd();
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SERVICENAME=SPEEDY;";
                    cmdList.add(cmd);

                    cmd = "CFG-ONUBW::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::UPBW=UP-" + bwUp + ",DOWNBW=DOWN-" + bwDown + ",SERVICENAME=SPEEDY;";
                    cmdList.add(cmd);

                    cmd = "CHG-PORTLOCATING::DID=" + ipGpon + ",OID=" + onuId + ":::VIFID=" + vport + ",FORMAT=dsl-forum;";
                    cmdList.add(cmd);

                    cmd = "CHG-PORT-PPPOEPLUS::DID=" + ipGpon + ",OID=" + onuId + ":::VIFID=" + vport + ",STATUS=enable;";
                    cmdList.add(cmd);

                    if (typeOnt == 1) {
                        cmd = "CHG-ONUWANIP::DID=" + ipGpon + ",OID=" + onuId + ":::IPHOSTID=1,PPPOEUSER=" + acc + ",PPPOEPWD=" + pwd + ",VID=" + vlan + ";";
                        cmdList.add(cmd);

                        cmd = "CHG-ONUWAN::DID=" + ipGpon + ",OID=" + onuId + ":::WANID=1,IPHOSTID=1,SERVICETYPE=Internet;";
                        cmdList.add(cmd);
                    }
                } else {
                    if (typeOnt == 1) {
                        cmd = "DLT-ONUWAN::DID=" + ipGpon + ",OID=" + onuId + ":::WANID=1,IPHOSTID=1;";
                        cmdList.add(cmd);

                        cmd = "DLT-ONUWANIP::DID=" + ipGpon + ",OID=" + onuId + ":::IPHOSTID=1;";
                        cmdList.add(cmd);
                    }

                    cmd = "CHG-PORT-PPPOEPLUS::DID=" + ipGpon + ",OID=" + onuId + ":::VIFID=" + vport + ",STATUS=disable;";
                    cmdList.add(cmd);

                    cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=200,UV=200,SERVICENAME=SPEEDY;";
                    cmdList.add(cmd);
                }
            }

            desc = status == 1 ? "CONFIG INTERNET" : "DELETE INTERNET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail voip(DtoVoip dtoVoip) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoVoip.getStatus();
        if (status == 1 || status == 2) {
            Integer vlan = dtoVoip.getVlan();
            Integer svlan = typeOnt == 1 ? dtoVoip.getVlan() : 100;
            Integer vport = dtoVoip.getVport();
            String cmd;
            if (status == 1) {
                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SERVICENAME=VOIP;";
                cmdList.add(cmd);

                cmd = "CFG-ONUBW::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::UPBW=UP-564KF5,DOWNBW=DOWN-564KF5,SERVICENAME=VOIP;";
                cmdList.add(cmd);

                if (typeOnt == 1) {
                    cmd = "CHG-PORT-DHCP::DID=" + ipGpon + ",OID=" + onuId + ":::VIFID=" + vport + ",OPTION82STAT=Enable;";
                    cmdList.add(cmd);

                    dtoVoip.getData().forEach(data -> {
                        Integer post = data.getPots();
                        String acc = data.getAcc();
                        String pwd = data.getPwd();

                        String cmdData = "CFG-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::PT=SIP,PHONENUMBER=" + acc + ",SIPREGDM=\"10.0.0.10::10.0.0.40\",SIPUSERNAME=" + acc + "@telkom.net.id,SIPUSERPWD=" + pwd + ",VOIPVLAN=" + vlan + ",CCOS=0,IPMODE=DHCP,IPHOSTID=2;";
                        cmdList.add(cmdData);
                    });
                }
            } else {
                if (typeOnt == 1) {
                    dtoVoip.getData().forEach(data -> {
                        Integer post = data.getPots();

                        String cmdData = "DEL-VOIPSERVICE::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=MAC,ONUID=" + onuId + ",ONUPORT=1-1-1-" + post + ":::;";
                        cmdList.add(cmdData);
                    });
                    cmd = "CHG-PORT-DHCP::DID=" + ipGpon + ",OID=" + onuId + ":::VIFID=" + vport + ",OPTION82STAT=Disable;";
                    cmdList.add(cmd);
                }

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=100,UV=100,SERVICENAME=VOIP;";
                cmdList.add(cmd);
            }
            desc = status == 1 ? "CONFIG VOIP" : "DELETE VOIP";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail iptv(DtoIptv dtoIptv) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoIptv.getStatus();
        if (status == 1 || status == 2) {
            String cmd;
            if (status == 1) {
                cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=111,UV=111,SERVICENAME=USEETV;";
                cmdList.add(cmd);

                cmd = "CFG-ONUBW::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::UPBW=UP-2253KA4,DOWNBW=DOWN-9012KA4,SERVICENAME=USEETV;";
                cmdList.add(cmd);

                if (typeOnt == 1) {
                    Arrays.stream(dtoIptv.getLan()).iterator().forEachRemaining(lan -> {
                        String cmdData = "ADD-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::MVLAN=110,SERVICENAME=USEETV;";
                        cmdList.add(cmdData);

                        cmdData = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::VLANMOD=Hybrid,PVID=111;";
                        cmdList.add(cmdData);

                        cmdData = "CHG-ONUUNI-PON::DID=" + ipGpon + ",OID=" + onuId + ",ONUPORT=" + lan + ":::IPRETRIEVEMODE=FromNetwork;";
                        cmdList.add(cmdData);
                    });
                } else {
                    cmd = "ADD-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::MVLAN=110,SERVICENAME=USEETV;";
                    cmdList.add(cmd);
                }
            } else if (status == 2) {
                if (typeOnt == 1) {
                    Arrays.stream(dtoIptv.getLan()).iterator().forEachRemaining(lan -> {
                        String cmdData = "CHG-ONUUNI-PON::DID=" + ipGpon + ",OID=" + onuId + ",ONUPORT=" + lan + ":::IPRETRIEVEMODE=NoControl;";
                        cmdList.add(cmdData);

                        cmdData = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::VLANMOD=None;";
                        cmdList.add(cmdData);

                        cmdData = "DEL-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::MVLAN=110,SERVICENAME=USEETV;";
                        cmdList.add(cmdData);
                    });
                } else {
                    cmd = "DEL-LANIPTVPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::MVLAN=110,SERVICENAME=USEETV;";
                    cmdList.add(cmd);
                }

                cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=111,UV=111,SERVICENAME=USEETV;";
                cmdList.add(cmd);
            }
            desc = status == 1 ? "CONFIG USEETV" : "DELETE USEETV";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }

    @Override
    public DtoCmdDetail other(DtoOther dtoOther) {
        List<String> cmdList = new ArrayList<>();
        String desc;
        DtoCmdDetail dtoCmdDetail = new DtoCmdDetail();
        Integer status = dtoOther.getStatus();
        if (status == 1 || status == 2) {
            dtoOther.getData().forEach(data -> {
                Integer vlan = data.getVlan();
                Integer svlan = typeOnt == 1 ? data.getVlan() : data.getSvlan();
                String name = data.getName();
                String cmd;
                if (status == 1) {
                    String bwUp = data.getBwUp();
                    String bwDown = data.getBwDown();
                    cmd = "ADD-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SERVICENAME=" + name + ";";
                    cmdList.add(cmd);

                    cmd = "CFG-ONUBW::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::UPBW=UP-" + bwUp + ",DOWNBW=DOWN-" + bwDown + ",SERVICENAME=" + name + ";";
                    cmdList.add(cmd);

                    if (typeOnt == 1) {
                        Arrays.stream(data.getLan()).iterator().forEachRemaining(lan -> {
                            String cmdData = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::VLANMOD=Hybrid,PVID=" + vlan + ";";
                            cmdList.add(cmdData);

                            cmdData = "CHG-ONUUNI-PON::DID=" + ipGpon + ",OID=" + onuId + ",ONUPORT=" + lan + ":::IPRETRIEVEMODE=FromNetwork;";
                            cmdList.add(cmdData);
                        });
                    }
                } else if (status == 2) {
                    if (typeOnt == 1) {
                        Arrays.stream(data.getLan()).iterator().forEachRemaining(lan -> {
                            String cmdData = "CHG-ONUUNI-PON::DID=" + ipGpon + ",OID=" + onuId + ",ONUPORT=" + lan + ":::IPRETRIEVEMODE=NoControl;";
                            cmdList.add(cmdData);

                            cmdData = "CFG-LANPORT::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ",ONUPORT=1-1-1-" + lan + ":::VLANMOD=None;";
                            cmdList.add(cmdData);
                        });
                    }

                    cmd = "DEL-PONVLAN::OLTID=" + ipGpon + ",PONID=1-1-" + slotPort + ",ONUIDTYPE=SN,ONUID=" + onuId + ":::CVLAN=" + vlan + ",UV=" + svlan + ",SERVICENAME=" + name + ";";
                    cmdList.add(cmd);
                }
            });
            desc = status == 1 ? "CONFIG WIFIID/VPNID/ASTINET" : "DELETE WIFIID/VPNID/ASTINET";
            dtoCmdDetail.setTarget(target);
            dtoCmdDetail.setDesc(desc);
            dtoCmdDetail.setCmds(cmdList);
        }
        return dtoCmdDetail;
    }
}

package id.co.gtx.sacserviceclient.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Data
@Builder
public class DtoCmdDetail implements Serializable {
    private static final long serialVersionUID = 6118121933374326609L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("target")
    private String target;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("desc")
    private String desc;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cmd")
    private String cmd;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cmds")
    private List<String> cmds;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("skip_error")
    private boolean skipError;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("time_sleep")
    private Integer timeSleep;

    public DtoCmdDetail() {
        this.skipError = false;
        this.timeSleep = 2;
    }

    public DtoCmdDetail(String desc, String cmd) {
        this.desc = desc;
        this.cmd = cmd;
    }

    public DtoCmdDetail(String desc, List<String> cmds, Integer timeSleep) {
        this.desc = desc;
        this.cmds = cmds;
        this.timeSleep = timeSleep;
    }
}

package id.co.gtx.sacserviceclient.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.dto.DtoNms;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DtoCmdBatch implements Serializable {
    private static final long serialVersionUID = -1605726232817438343L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("nms")
    private DtoNms nms;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("login")
    private String login;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("logout")
    private String logout;

    @JsonProperty("details")
    List<DtoCmdBatchDetail> details;
}

package id.co.gtx.sacserviceclient.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DtoCmdBatchDetail implements Serializable {
    private static final long serialVersionUID = -4084209403318597369L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("status")
    private Integer Status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("ont")
    private DtoCmdDetail ont;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("inet")
    private DtoCmdDetail inet;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("voip")
    private DtoCmdDetail voip;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("iptv")
    private DtoCmdDetail iptv;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("other")
    private DtoCmdDetail other;
}

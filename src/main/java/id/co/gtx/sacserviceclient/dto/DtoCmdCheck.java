package id.co.gtx.sacserviceclient.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.co.gtx.sacmodules.dto.DtoConfig;
import id.co.gtx.sacmodules.dto.DtoNms;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DtoCmdCheck implements Serializable {
    private static final long serialVersionUID = 7853734704182674008L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nms")
    private DtoNms nms;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("login")
    private String login;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("logout")
    private String logout;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cmd_type")
    private String cmdType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("cmd_list")
    private DtoCmdDetail cmdList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("gpon")
    private List<DtoConfig> gpon;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("ip_gpon")
    private List<String> ipGpon;
}

package id.co.gtx.sacserviceclient.controller;

import id.co.gtx.sacserviceclient.service.ConfigService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigController {

    private final ConfigService configService;

    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }

    @GetMapping("/download/template/{filename:.+}")
    public ResponseEntity<Resource> downloadTemplate(@PathVariable String filename) {
        return configService.downloadTemplate(filename);
    }
}

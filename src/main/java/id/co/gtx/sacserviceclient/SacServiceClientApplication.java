package id.co.gtx.sacserviceclient;

import id.co.gtx.sacserviceclient.config.properties.FileStorageProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;

@RefreshScope
@SpringBootApplication
@ComponentScan(basePackages = {"id.co.gtx"})
@EnableDiscoveryClient
@EnableConfigurationProperties({FileStorageProperties.class})
@OpenAPIDefinition(info =
@Info(title = "Config Service API",
        version = "${spring.application.version}",
        description = "Documentation Config Service API"))
public class SacServiceClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SacServiceClientApplication.class, args);
    }
}
